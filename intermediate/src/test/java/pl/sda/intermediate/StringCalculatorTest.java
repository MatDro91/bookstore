package pl.sda.intermediate;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringCalculatorTest {
    @Test
    void shouldReturnZeroWhenTextIsEmpty() {
        String exampleData = "";

        int result = StringCalculator.adding(exampleData);
        assertEquals(0, result);
    }

    @Test
    void shouldReturnZeroWhenTextIsBlank() {
        String exampleData = " ";

        int result = StringCalculator.adding(exampleData);
        assertEquals(0, result);
    }

    @Test
    void shouldReturnNumberWhenDataIsOneNumber() {
        String exampleData = "7";
        String exampleData1 = "7 ";

        int result = StringCalculator.adding(exampleData);
        int result1 = StringCalculator.adding(exampleData);
        assertEquals(7, result);
        assertEquals(7, result1);
    }

    @Test
    void shouldReturnSumWhenDataHasDelimiter() {
        String exampleData = "1,2 ";

        int result = StringCalculator.adding(exampleData);


        assertEquals(3, result);
    }

    @Test
    void shouldReturnSumWhenDataHasDelimiterMoreThanTwo() {
        String exampleData = "1,2 , 5";
        int result = StringCalculator.adding(exampleData);

        assertEquals(8, result);
    }

    @Test
    void shouldReturnSumOfMannyNumberSeperatedWithCustomDelimiter() {
        String exampleData = "//;\n1;2";
        int result = StringCalculator.adding(exampleData);

        assertEquals(3, result);
    }

}