package pl.sda.intermediate.bookStore.products;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

class ProductDaoTest {
    @Test
    void shouldPopulateProducstList() {
        Product product = new Product("Lameness of the Horse, by John Victor Lacroix", 16370);
        product.setDescription("[Subtitle: Veterinary Practitioners' Series, No. 1]");

        ProductDao productDao = new ProductDao();
        List<Product> productList = productDao.getProductList("", Integer.MAX_VALUE);

        Product productToMatch = productList.stream()
                .filter(p -> p.getId() == 16370)
                .findFirst().orElseThrow(() -> new RuntimeException("Nie znaleziono produktu"));
        Assertions.assertEquals(productToMatch.getDescription(), product.getDescription());
        Assertions.assertEquals(product.getTitle(), product.getTitle());
    }
}