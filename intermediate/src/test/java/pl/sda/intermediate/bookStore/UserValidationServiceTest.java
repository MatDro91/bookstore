package pl.sda.intermediate.bookStore;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pl.sda.intermediate.bookStore.users.entities.UserAddress;
import pl.sda.intermediate.bookStore.users.dtos.UserRegistrationDTO;
import pl.sda.intermediate.bookStore.users.services.UserValidationService;

import java.util.Map;

class UserValidationServiceTest {

    @Test
    void shouldNotPassUserWithBlankFirstName() {
        UserRegistrationDTO userRegistrationDTO = populateValidUser();
        userRegistrationDTO.setFirstName(" ");
        UserValidationService userValidationService=new UserValidationService();
        Map<String, String> errorsMap = userValidationService.validateUserData(userRegistrationDTO);

        Assertions.assertTrue(errorsMap.containsKey(UserValidationService.FIRST_NAME_VAL_RES));

    }

    @Test
    void shouldNotPassUserWithTooShortName() {
        UserRegistrationDTO userRegistrationDTO = populateValidUser();
        userRegistrationDTO.setFirstName("Bn ");
        UserValidationService userValidationService=new UserValidationService();
        Map<String, String> errorsMap = userValidationService.validateUserData(userRegistrationDTO);

        Assertions.assertTrue(errorsMap.containsKey(UserValidationService.FIRST_NAME_VAL_RES));
    }

    private UserRegistrationDTO populateValidUser() {
        UserAddress userAddress = new UserAddress();
        userAddress.setCity("Lodz");
        userAddress.setCountry("Polska");
        userAddress.setStreet("Długa");
        userAddress.setZipCode("95-000");
        UserRegistrationDTO userRegistrationDTO = new UserRegistrationDTO();

        userRegistrationDTO.setFirstName("Anna");
        userRegistrationDTO.setLastName("Kowalska");
        userRegistrationDTO.setBirthDate("1995-05-09");
        userRegistrationDTO.setPesel("12345678911");
        userRegistrationDTO.setEmail("meil@mail.com");
        userRegistrationDTO.setPhone("666-999-333");
        userRegistrationDTO.setPassword("DupaDupaDupa");
        userRegistrationDTO.setUserAddress(userAddress);
        return userRegistrationDTO;
    }
}