package pl.sda.intermediate;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;
@Setter
@Getter
public class Pair<T, E> {
    private T obj1;
    private E obj2;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(obj1, pair.obj1) &&
                Objects.equals(obj2, pair.obj2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(obj1, obj2);
    }
}
