package pl.sda.intermediate;

import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class Generics {
    //Napisz generyczną metodę która przyjmuje Listę jakichkolwiek elementów
    // i wypisuje wszystkie elementy tej listy

    @Test
    void test1() {
        printAndSort(Lists.newArrayList("a", "c", "b"), (a, b) -> a.compareTo(b));
        printAndSort(Lists.newArrayList(1, 11, 7), (a, b) -> a.toString().compareTo(b.toString()));
        sumNumbers(Lists.newArrayList(22, 11, 18, 34), e -> e > 4);
    Pair<Integer,String> pair1=new Pair<>();
    Pair<Integer,String> pair2=new Pair<>();

    pair1.setObj1(1);
    pair2.setObj1(1);
    pair1.setObj2("1");
    pair2.setObj2("1");
    }


    private <E> void printEverything(List<E> list) {
        for (E element : list) {
            System.out.println(element.toString());
        }
    }
    //Napisz generyczną metodę która przyjmuje Listę jakichkolwiek elementów i wypisuje
    // wszystkie elementy tej listy, ale z zachowaniem kolejności przekazanej w parametrze

    private <E extends Comparable> void printAndSort(List<E> list, Comparator<E> comparator) {
        list.stream()
                .sorted(comparator)
                .forEach(e -> System.out.println(e));
    }

    private <E extends Number> void sumNumbers(List<E> list, Predicate<Integer> integerPredicate) {
        list.stream()
                .map(e -> e.intValue())
                .filter(integerPredicate)
                .reduce((a, b) -> a + b)
                .orElse(0);
    }

    private boolean compareTwoPairs(Pair pair1, Pair pair2) {
        return pair1.equals(pair2);
    }
}
