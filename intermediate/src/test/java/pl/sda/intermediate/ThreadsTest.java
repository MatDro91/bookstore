package pl.sda.intermediate;

import org.junit.jupiter.api.Test;
import pl.sda.intermediate.threads.CustomProcessThread;

import java.util.concurrent.CompletableFuture;

public class ThreadsTest {
    @Test
    void threads() throws InterruptedException {
        Thread customProcessThread = new CustomProcessThread();// w ten sposób od razu tworzymy wątek
        customProcessThread.start();

        Thread thread = new Thread(new CustomProcessRunnable());// w przypadku runnable musimy utworzyć wątek
        thread.start();

        customProcessThread.join();
        thread.join();

    }
}
