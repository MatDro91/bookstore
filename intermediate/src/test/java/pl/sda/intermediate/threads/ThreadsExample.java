package pl.sda.intermediate.threads;

import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ThreadsExample {

    @Test
    void bankWithTreads() {
        List<BankClientAcction> clientsActions = Lists.newArrayList();
        for (int i = 0; i < 1000; i++) {
            clientsActions.add(new BankClientAcction());
        }
        List<Thread> threads = Lists.newArrayList();
        for (BankClientAcction clientsAction : clientsActions) {
            threads.add(new Thread(clientsAction));
        }

        for (Thread thread : threads) {
            thread.start();// uruchmiamy zadania
        }

        for (Thread thread : threads) {
            try {
                thread.join();// czekamy na zakończenie pracy
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Ilość operacji " + Bank.operations);
        System.out.println("Stan Konta  " + Bank.balance);


    }

    @Test
    void runnableBasics() {
        Runnable ourRunnable = new OurRunnable();// zadanie

        Runnable lambdaRunnable = () -> System.out.println(Thread.currentThread().getName() + " Lambda");

        Runnable anonymous = new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + " Anonimowa klasa");
            }
        };

        ourRunnable.run();
        anonymous.run();
        lambdaRunnable.run();

        Thread firstThread = new Thread(ourRunnable);
        Thread secndThread = new Thread(anonymous);
        Thread thirdThread = new Thread(lambdaRunnable);

        ArrayList<Thread> threads = Lists.newArrayList(firstThread, secndThread, thirdThread);
        for (Thread thread : threads) {
            thread.start();
        }
    }
}
