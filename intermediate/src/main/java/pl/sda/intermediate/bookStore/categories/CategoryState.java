package pl.sda.intermediate.bookStore.categories;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class CategoryState {
    private boolean open;
    private boolean selected;
    private boolean disabled;
}
