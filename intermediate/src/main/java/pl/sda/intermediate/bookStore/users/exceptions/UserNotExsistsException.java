package pl.sda.intermediate.bookStore.users.exceptions;

public class UserNotExsistsException extends RuntimeException {
    public UserNotExsistsException(String message) {
        super(message);
    }
}
