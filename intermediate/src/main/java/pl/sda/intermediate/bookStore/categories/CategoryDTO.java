package pl.sda.intermediate.bookStore.categories;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CategoryDTO {

    private String id;
    private String text;
    private CategoryState state;
    private String parentCategoryID;
    private CategoryDTO parentCat;

    public String getParent() {
        if (parentCategoryID == null) { // return parentCategoryID == null ? "#" : parentCategoryID;
            return "#";
        }
        return parentCategoryID;
    }

}
