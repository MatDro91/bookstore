package pl.sda.intermediate.bookStore.users.dtos;

import lombok.Data;

@Data
public class UserLoggedInDto {
    private String login;

    public UserLoggedInDto(String email) {
        this.login = email;
    }
}
