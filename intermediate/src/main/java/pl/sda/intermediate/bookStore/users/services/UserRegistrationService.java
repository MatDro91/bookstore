package pl.sda.intermediate.bookStore.users.services;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.intermediate.bookStore.users.daos.UserDAO;
import pl.sda.intermediate.bookStore.users.dtos.UserRegistrationDTO;
import pl.sda.intermediate.bookStore.users.entities.User;
import pl.sda.intermediate.bookStore.users.exceptions.userExistsException;

@Service
public class UserRegistrationService {
    @Autowired
    private UserDAO userDAO;

    public void registerUser(UserRegistrationDTO userRegistrationDTO){
        if (userExsits(userRegistrationDTO)){
            throw new userExistsException("Użytkownik istnieje");
        }
        User user= rewriteDTOToUser(userRegistrationDTO);
        userDAO.addUser(user);
    }

    private User rewriteDTOToUser(UserRegistrationDTO userRegistrationDTO) {
        User user= new User();

        user.setFirstName(userRegistrationDTO.getFirstName());
        user.setLastName(userRegistrationDTO.getLastName());
        user.setZipCode(userRegistrationDTO.getUserAddress().getZipCode());
        user.setCity(userRegistrationDTO.getUserAddress().getCity());
        user.setCountry(userRegistrationDTO.getUserAddress().getCountry());
        user.setStreet(userRegistrationDTO.getUserAddress().getStreet());
        user.setBirthDate(userRegistrationDTO.getBirthDate());
        user.setPesel(userRegistrationDTO.getPesel());
        user.setEmail(userRegistrationDTO.getEmail());
        user.setPassword(DigestUtils.sha512Hex(userRegistrationDTO.getPassword()));
        user.setPhone(userRegistrationDTO.getPhone());
        user.setPreferEmails(userRegistrationDTO.isPreferEmails());

        return user;
    }

    private boolean userExsits(UserRegistrationDTO userRegistrationDTO) {
        return userDAO.findUserByEmail(userRegistrationDTO.getEmail()).isPresent();
    }
}
