package pl.sda.intermediate.bookStore.users.services;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.intermediate.bookStore.users.daos.UserDAO;
import pl.sda.intermediate.bookStore.users.dtos.UserLoginDTO;
import pl.sda.intermediate.bookStore.users.entities.User;
import pl.sda.intermediate.bookStore.users.exceptions.PasswordDoesNotMatchesException;
import pl.sda.intermediate.bookStore.users.exceptions.UserNotExsistsException;

import java.util.Optional;

@Service
public class UserLoginService {
    @Autowired
    private UserDAO userDAO;


    public void login(UserLoginDTO userLoginDTO) {
        Optional<User> userByEmail = userDAO.findUserByEmail(userLoginDTO.getLogin());
        if (!userByEmail.isPresent()) {
            throw new UserNotExsistsException("Nie ma takiego użytkownika " + userLoginDTO.getLogin());
        }
        if (passwordDoesNotMatch(userLoginDTO, userByEmail)) {
            throw new PasswordDoesNotMatchesException("Błędne hasło");
        }
        UserContextHolder.logInUser(userByEmail.get().getEmail());
    }

    private boolean passwordDoesNotMatch(UserLoginDTO userLoginDTO, Optional<User> userByEmail) {
        return !DigestUtils.sha512Hex(userLoginDTO.getPassword()).equals(userByEmail.get().getPassword());
    }
}
