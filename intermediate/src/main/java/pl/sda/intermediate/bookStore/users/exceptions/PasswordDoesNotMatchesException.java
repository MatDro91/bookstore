package pl.sda.intermediate.bookStore.users.exceptions;

public class PasswordDoesNotMatchesException extends RuntimeException {
    public PasswordDoesNotMatchesException(String message) {
        super(message);
    }
}
