package pl.sda.intermediate.bookStore.users.entities;

import lombok.Getter;

@Getter
public enum CountryEnum {
    POLAND("PL", "Polska"),
    ENGLAND("ENG", "Anglia"),
    RUSSIA("RU", "Rosja"),
    GERMANY("DE", "Niemcy"),
    FRANCE("F", "Francja");

    private String symbol;
    private String plName;

    CountryEnum(String symbol, String plName) {
        this.symbol = symbol;
        this.plName = plName;
    }
}
