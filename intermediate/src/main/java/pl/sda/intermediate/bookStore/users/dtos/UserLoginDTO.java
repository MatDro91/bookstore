package pl.sda.intermediate.bookStore.users.dtos;

import lombok.Data;

@Data
public class UserLoginDTO {
    private String login;
    private String password;
}
