package pl.sda.intermediate.bookStore.users.services;

import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import pl.sda.intermediate.bookStore.users.dtos.UserRegistrationDTO;

import java.util.Map;
@Service
public class UserValidationService {

    public static final String FIRST_NAME_VAL_RES = "firstNameValRes";
    public static final String LAST_NAME_VAL_RES = "lastNameValRes";
    public static final String ZIP_CODE_VAL_RES = "zipCodeValRes";
    public static final String COUNTRY_VAL_RES = "countryValRes";
    public static final String STREET_VAL_RES = "streetValRes";
    public static final String CITY_VAL_RES = "cityValRes";
    public static final String PESEL_NUMBER_VAL_RES = "peselValRes";
    public static final String BIRTH_DATA_VAL_RES = "birthDataValRes";
    public static final String EMAIL_VAL_RES = "emailValRes";
    public static final String PASSWORD_VAL_RES = "passwordValRes";
    public static final String PHONE_VAL_RES = "phoneValRes";

    public Map<String, String> validateUserData(UserRegistrationDTO userRegistrationDTO) {
        Map<String, String> resultMap = Maps.newHashMap();
        if (namePartIsNotValid(userRegistrationDTO.getFirstName(), "[A-Z][a-z]{2,}")) {
            resultMap.put(FIRST_NAME_VAL_RES, "Imię jest wymaagane, musi zaczynać się z wielkiej musi zawierać przynajmniej trzy litery");
        }
        if (namePartIsNotValid(userRegistrationDTO.getLastName(), "[A-Z][a-z]{2,}(|-[A-Z][a-z]{2,})")) {
            resultMap.put(LAST_NAME_VAL_RES, "Nazwisko jest wymaagane, musi zaczynać się z wielkiej musi zawierać przynajmniej trzy litery");
        }
        if (zipCodeIsNotValid(userRegistrationDTO.getUserAddress().getZipCode(), "[0-9]{2}-[0-9]{3}")) {
            resultMap.put(ZIP_CODE_VAL_RES, "Kod pocztowy jest wymagany");
        }
        if (addressPartIsNotValid(userRegistrationDTO.getUserAddress().getCity())) {
            resultMap.put(CITY_VAL_RES, "Pole nie może być puste");
        }
        if (addressPartIsNotValid(userRegistrationDTO.getUserAddress().getCountry())) {
            resultMap.put(COUNTRY_VAL_RES, "Kraj jest wymagany, pole nie może być puste");
        }
        if (addressPartIsNotValid(userRegistrationDTO.getUserAddress().getStreet())) {
            resultMap.put(STREET_VAL_RES, "Ulica jest wymagana, pole nie może być puste");
        }
        if (dateOfbirthIsNotValid(userRegistrationDTO.getBirthDate(), "^(19[0-9]{2}|20[0-1][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$")) {
            resultMap.put(BIRTH_DATA_VAL_RES, "Błędny format, data powinna być podana RRRR-MM-DD");
        }
        if (peselNumberIsNotValid(userRegistrationDTO.getPesel(), "[0-9]{11}")) {
            resultMap.put(PESEL_NUMBER_VAL_RES, "Błędny numer PESEL");
        }
        if (addressEmailIsNotValid(userRegistrationDTO.getEmail(),"^[\\w]+[@][\\w]+[\\.][a-z]{2,}$" )) {     //"([a-z0-9]+([\\.\\_])?[a-z0-9]+)+@([a-z0-9]+([\\.\\_])?[a-z0-9]+)+\\.[a-z]{2,}"
            resultMap.put(EMAIL_VAL_RES, "Błędny email");
        }
        if (passwordIsNotValid(userRegistrationDTO.getPassword(), "^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{10,20})$")) {
            resultMap.put(PASSWORD_VAL_RES, "Błędne hasło. Hasło musi zawierać przynajmniej jedną dużą literę i jedną cyfre oraz musi się składać z minimum 10 znaków.");
        }
        if (telephoneNumberIsNotValid(userRegistrationDTO.getPhone(), "^(\\+48|)( |)([1-9][0-9]{2}-){2}[0-9]{3}|[0-9]{9}")) {
            resultMap.put(PHONE_VAL_RES, "Podano błędny numer telefonu");
        }
        return resultMap;
    }

    private boolean telephoneNumberIsNotValid(String phone, String regex) {
        return isaBoolean(phone, regex);
    }

    private boolean passwordIsNotValid(String password, String regex) {
        return isaBoolean(password, regex);
    }

    private boolean addressEmailIsNotValid(String email, String regex) {
        return isaBoolean(email, regex);
    }

    private boolean peselNumberIsNotValid(String pesel, String regex) {
        return isaBoolean(pesel, regex);
    }

    private boolean dateOfbirthIsNotValid(String birthDate, String regex) {
        return isaBoolean(birthDate, regex);// problem w przypadku lutego oraz 30 31 dni
    }

    private boolean addressPartIsNotValid(String city) {
        return StringUtils.isBlank(city);
    }

    private boolean zipCodeIsNotValid(String zipCode, String regex) {

        return isaBoolean(zipCode, regex);
    }

    private boolean namePartIsNotValid(String sameName, String regex) {
        return isaBoolean(sameName, regex);
    }

    private boolean isaBoolean(String string, String regex) {
        return StringUtils.isBlank(string)
                || !string.trim().matches(regex);
    }
}
